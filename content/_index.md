---
title: Home
menu: sidebar
weight: 1
---

## Cinc

A Free-as-in-Beer distribution of the open source software of Chef Software Inc. See [goals]({{< ref "goals.md" >}}) for details, or follow our [blog]({{< ref "blog/_index.md" >}}) for updates on the project.

The Cinc team is proud to present:

### Cinc Projects

- [Cinc Client]({{< relref "client.md" >}}), built from Chef Infra&trade;
- [Cinc Workstation]({{< relref "workstation.md" >}}) (_pre-release_) , built from Chef Workstation&trade;
- [Cinc Auditor]({{< relref "auditor.md" >}}), built from Chef InSpec&trade;

### Coming Soon

- [Cinc Server]({{< ref "download#cinc-server" >}}), built from Chef Chef Infra Server&trade;
- [Cinc Packager]({{< ref "download#cinc-packager" >}}), built from Chef Habitat&trade;

Come chat with us! [community slack](http://community-slack.chef.io/) channel #community-distros
