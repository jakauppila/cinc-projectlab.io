---
title: "Workstation"
draft: false
date: 2020-04-10T15:31:30-05:00
menu:
  sidebar:
    parent: Getting Started
slug: start/workstation
weight: 10
---

## Workstation Install

_Workstation builds are currently alpha_

The Cinc Workstation builds have not yet completed EULA review with the [Chef Software Trademark Policy](https://www.chef.io/trademark-policy/) and may contain crippling bugs. You can however already find unstable builds of Cinc Workstation so our users can help us locate any issues with it.

{{% gs_omnitruck_install version="0.17.x" product="Cinc Workstation" omnitruckParams="-P cinc-workstation -v 0.17 -c unstable" omnitruckParamsWindows="-project cinc-workstation -version 0.17 -channel unstable" %}}

{{% gs_package_install channel="unstable" package="cinc-workstation" %}}

### Included tools

While Cinc Workstation contains all the same tools as Chef Workstation&trade;, some of them had to be modified for compliance:

- mixlib-install serves Cinc Products
- Chef Infra&trade; --> Cinc Client
  - `chef-zero` --> `cinc-zero`
  - `chef-solo` --> `cinc-zero`
  - `chef-apply` --> `cinc-apply`
  - `chef-shell` --> `cinc-shell`
- Chef Inspec&trade; --> Cinc Auditor

We're still working on making trademark compliant versions of the following:

- `chef-analyze`
- `chef`/`chef-cli`
- The Chef Workstation app

### Configuration

Cinc uses the `~/.cinc-workstation` folder for it's configuration on Unix-like systems and `%USERPROFILE%\.cinc-workstation` on Windows.

Users migrating from Chef will probably want to retain most of their configuration. All Cinc configurations are compatible with their Chef counterparts so you can simply use a symlink or copy the contents as-is.

## Usage

Cinc Workstation's tools are fully compatible with their Chef Workstation counterparts. Many are in fact not modified at all. We include wrappers for Chef Workstation binaries we've renamed to the Cinc binaries. This should facilitate compatibility with your existing automation.

All [upstream documentation](https://docs.chef.io) remains valid, and so are the [classes provided by Chef Software Inc.](https://learn.chef.io)

{{% gs_come_see_us %}}
