---
date: "2020-05-10T12:41:55-04:00"
authors: ["bobchaos"]
title: "Breaking changes for redistributors"
tags:
  - Cinc
  - Upstream
---

## The short version

This blog post is intended specifically for other folks that are building their own distributions of Chef Software's product, and folks leveraging the tip from my [previous blog]({{< relref "cooking_with_cinc" >}}) in their cookbooks.

We're preparing to completely rewrite the current implementation of `dist.rb` in both [Chef Infra](https://github.com/chef/chef) and [Chef Inspec](https://github.com/inspec/inspec). The short version is that we're moving all the constants like `Chef::Dist::PRODUCT` to a new namespace and gem, respectively `ChefUtils::Dist` in the `chef-utils` gem. By moving dist.rb in there, we can effectively maintain all the trademark constants in a single, central gem.

Do note this only applies to Ruby-based tools. Go-based tools will continue using the [code generator](https://github.com/chef/go-libs/tree/master/distgen).

## The long version

One of the Cinc Project's defined [goals]({{< ref "goals.md" >}}) is to make Chef Software Inc's products easily distributable under a configurable name. We've dubbed the feature simply "dist" for short. The initial dist implementation was done a little bit haphazardly as both the project and Chef's product teams were exploring this new reality of trademark compliance. When we started, as most projects do, we didn't quite know how this would all turn out.

As a result, there was a lack of coherence in naming and each repo had it's own implementation of the dist constants that we use to substitute Chef's trademarks. In chef/chef, we even ended up with 2 distinct implementations of dist.rb. In short, it had slowly evolved into something difficult to maintain, both for us as main contributors on the feature and for the maintainers, despite the concept's simplicity.

`chef-utils` is a fairly recent addition to the Chef Infra codebase. While it's source lives in chef/chef, it is a distinct rubygem published alongside `chef`. It's intended to be a "foundations" ruby library use by Chef Software for low level helpers that are common in the ecosystem like `.windows?`. That made it the perfect place to unify all our constants.

Thankfully, we'd already done the work of substituting all references in code to trademarks with unique constants. This allowed aggressive strategies in migrating to the new implementation. I used [codemod](https://github.com/facebook/codemod), a mass refactor CLI tool, to accomplish this quickly while still getting a human's attention on each change.

The resulting unified implementation can be seen here: https://github.com/chef/chef/pull/9834

As I'm writing this, that PR is marked as WIP to prevent it being merged before this blog post has a chance to be read by our intended audience. Once we have it merged (That should happen before Chef Infra 16.1 is released upstream), we'll proceed to modifying every other ruby Chef Software repo where we've implemented dist to use `chef-utils`, so be aware that there may be more breaking changes in the dist implementation of other products Cinc redistributes. This should all be confined to a short time span, and we hope this will be the final implementation of dist.

This of course doesn't mean that we're done with upstream contributions! We still find the occasional wordmark that slipped by, or was erroneously introduced since we first did all this, so we have to remain vigilant.

I sincerely hope this won't cause any issues for anyone, but if so please visit the PR I linked above and make it known.
